package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);

        
        
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



        List<String> messagess = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messagess) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setBranches(request.getParameter("branches"));
            user.setPotision(request.getParameter("potision"));
            user.setUsers(request.getParameter("users"));
            user.setDescription(request.getParameter("description"));

            new UserService().register(user);



            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messagess);
            response.sendRedirect("signup");
        }
    }




    private boolean isValid(HttpServletRequest request, List<String> messagess) {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String branches = request.getParameter("branches");
        String potision = request.getParameter("potision");
        String users = request.getParameter("users");





        if (StringUtils.isEmpty(account) == true) {
            messagess.add("アカウント名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messagess.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(branches) == true) {
            messagess.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(potision) == true) {
            messagess.add("部署・役職名を入力してください");
        }
        if (StringUtils.isEmpty(users) == true) {
            messagess.add("ユーザー名を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messagess.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}