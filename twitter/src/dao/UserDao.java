package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import utils.DBUtil;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO userd ( ");//mysqlのデータベースのテーブル名。
            sql.append("account");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branches");
            sql.append(", potision");
            sql.append(", users");
            sql.append(", description");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branches
            sql.append(", ?"); // potision
            sql.append(", ?");//users
            sql.append(", ?"); // description
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getBranches());
            ps.setString(5, user.getPotision());
            ps.setString(6, user.getUsers());
            ps.setString(7, user.getDescription());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String account,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM userd WHERE (account = ?) AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private static List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String branches = rs.getString("branches");
                String potision = rs.getString("potision");
                String users = rs.getString("users");
                String description = rs.getString("description");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setPassword(password);
                user.setBranches(branches);
                user.setPotision(potision);
                user.setUsers(users);
                user.setDescription(description);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM userd WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE userd SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            sql.append(", branches = ?");
            sql.append(", potision = ?");
            sql.append(", users = ?");
            sql.append(", password = ?");
            sql.append(", description = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getBranches());
            ps.setString(4, user.getPotision());
            ps.setString(5, user.getUsers());
            ps.setString(6, user.getPassword());
            ps.setString(7, user.getDescription());
            ps.setInt(8, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
	public static List<User> getUserList(Connection connection){
		String sql = "SELECT * FROM userd WHERE branches  AND potision ";
		ResultSet rs = null;
		try {
			rs = connection.createStatement().executeQuery(sql);

			List<User> userList = toUserList(rs);
			return userList;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}finally{
			DBUtil.close(rs);
			DBUtil.close(connection);
		}
	}
	public User getUser2(Connection connection, String account,
            String password,String name,String branches,String potision,String users) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM  userd_branches_potision WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);
            ps.setString(3, name);
            ps.setString(4, branches);
            ps.setString(5, potision);
            ps.setString(6, users);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
          //  } else if (2 <= userList.size()) {
         //       throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}