<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
    </head>
    <body>
 <div class="header">
    <c:if test="${ empty loginUser }">
        <a href="login">ログイン</a>
        <a href="signup">登録する</a>
    </c:if>
    <c:if test="${ not empty loginUser }">
        <a href="userManagement">ユーザー管理</a>
        <a href="newcomment">新規投稿</a>

    </c:if>
</div>
<c:if test="${ not empty loginUser }">
    <div class="profile">
        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>

        <div class="account">
            <c:out value="${loginUser.account}" />
        </div>

          <div class="account">
            <c:out value="${loginUser.branches}" />
        </div>

        <div class="account">
            <c:out value="${loginUser.potision}" />
        </div>

         <div class="account">
            <c:out value="${loginUser.users}" />
        </div>

        <div class="account">
            <c:out value="${loginUser.description}" />
        </div>
    </div>
</c:if>

<%--コメントアウト
<div class="form-area">

        <form action="newMessage" method="post">
        <span>タイトル</span><br>
		<input type ="text" name ="title" value ="${newPost.title }"><br>
           本文<br />
            <textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
            <br />

 <span>カテゴリー</span><br>
		<input type ="text" name = category  value ="${newCategory }"><br>
            <input type="submit" value="投稿">（140文字まで）


        </form>
</div>

コメントアウト--%>
<br>
<div class="messages">
    <c:forEach items="${messages}" var="message">
            <div class="message">
                <div class="account-name">
                   <span class="account">投稿者名&nbsp;&nbsp;<c:out value="${message.account}" /></span>
                    <span class="name">名前&nbsp;&nbsp;<c:out value="${message.name}" /></span>
                </div><br>
                <div class="title">タイトル&nbsp;&nbsp;<c:out value="${message.title}" /></div>
                <div class="text">本文&nbsp;&nbsp;<c:out value="${message.text}" /></div>
                <div class="category"> カテゴリ&nbsp;&nbsp;<c:out value="${message.category}" /></div>
              <div class="date">投稿日時&nbsp;&nbsp;<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            </div><br>



			<div class ="comment">
				<div class = "newComment">
					<label>コメント新規投稿</label><br>
					<form action="newMessage" method ="post">
						<input type ="hidden" name ="targetContributionId" value ="${contribution.id }">
						<c:if test="${contribution.getId().equals(newComment.getContributionId()) }">
							<c:set var = "newText" value ="${newComment.text}"></c:set>
						</c:if>
						<textarea rows="10" cols="40" name="text"><c:out value="${newText}"></c:out></textarea><br>
						<input type ="submit" class ="bottun" value ="コメントする">
						<c:remove var="newText"/>
					</form>
				</div>

				</div>

            <br>
    </c:forEach>
</div>
            <div class="copyright"> Copyright(c)柳田慶一</div>

    </body>
</html>
