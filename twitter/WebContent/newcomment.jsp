<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿</title>
    </head>
    <body>

    <h1>新規投稿</h1>
    <div class="header">
        <a href="./">ホーム</a>
    </div>
<div class="form-area">

        <form action="newMessage" method="post">
        <span>タイトル</span><br>
		<input type ="text" name ="title" value ="${newPost.title }"><br>
           本文<br />
            <textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
            <br />

 <span>カテゴリー</span><br>
		<input type ="text" name = category  value ="${newCategory }"><br>
            <input type="submit" value="投稿">（140文字まで）


        </form>
</div>
    </body>
</html>
