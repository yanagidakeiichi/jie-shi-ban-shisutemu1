<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板システム</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<h1>掲示板</h1>
		<div class="main-contents">
			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="settings">設定</a>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>

			<c:if test="${ not empty loginUser }">
			    <div class="profile">

			        <div class="name">アカウント名<h2><c:out value="${loginUser.name}" /></h2></div>
			        <div class="account">
			        メールアドレス
			            @<c:out value="${loginUser.account}" />
			        </div>
			        <div class="account">
			            <c:out value="${loginUser.description}" />
			        </div>
			    </div>
			</c:if>


			<div class="form-area">
			<c:if test="${ isShowMessageForm }">
			<input type="radio" name="example" value="選択肢1">選択肢1
<input type="radio" name="example" value="選択肢2" checked>選択肢2
<input type="radio" name="example" value="選択肢3">選択肢3
<br>
<input type="checkbox" name="example" value="サンプル">サンプル
<input type="checkbox" name="example" value="サンプル" checked>サンプル
<input type="checkbox" name="example" value="サンプル">サンプル
<br>
<select name="example">
    <option value="選択肢1">選択肢1</option>
    <option value="選択肢2">選択肢2</option>
    <option value="選択肢3">選択肢3</option>
</select>

					<form action="newMessage" method="post">
						件名<br />
						<textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
						<br />
						<input type="submit" value="つぶやく">（140文字まで）
					</form>
				</c:if>




				<c:if test="${ isShowMessageForm }">
					<form action="newMessage" method="post">
						投稿<br />
						<textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
						<br />
						<input type="submit" value="つぶやく">（140文字まで）
					</form>
				</c:if>
			</div>

			<div class="messages">
			<h2>投稿一覧</h2>
			    <c:forEach items="${messages}" var="message">
			            <div class="message">
			                <div class="account-name">
			                投稿者アカウント
			                    <span class="account"><c:out value="${message.account}" /></span>
			                    投稿者名
			                    <span class="name"><c:out value="${message.name}" /></span>
			                </div>
			                投稿内容
			                <div class="text"><c:out value="${message.text}" /></div>
			                投稿日時
			                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			            </div>
			    </c:forEach>
			</div>
			<div class="copylight"> Copyright(c)keiichi_yanagida</div>
		</div>
	</body>
</html>